module circleAccess

go 1.18

require (
	github.com/brianvoe/gofakeit/v6 v6.16.0
	github.com/google/go-github v17.0.0+incompatible
	github.com/gorilla/mux v1.8.0
	golang.org/x/oauth2 v0.4.0
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	golang.org/x/net v0.5.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.28.0 // indirect
)
