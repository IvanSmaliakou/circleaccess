package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/go-github/github"
	"golang.org/x/oauth2"
	"io"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/gorilla/mux"
)

var cfg = &Config{}

type Config struct {
	RepoOwner        string `json:"repo_owner"`
	BasicUser        string `json:"basic_user"`
	BasicPass        string `json:"basic_pass"`
	Email            string `json:"email"`
	CircleToken      string `json:"circle_token"`
	ProtoAndHostname string `json:"proto_and_hostname"`
	GhToken          string `json:"gh_token"`
	CircleURL        string `json:"circle_url"`
}

func (cfg *Config) HasNullValues() bool {
	return cfg.RepoOwner == "" || cfg.BasicUser == "" || cfg.BasicPass == "" ||
		cfg.Email == "" || cfg.CircleToken == "" || cfg.ProtoAndHostname == "" || cfg.GhToken == ""
}

type CircleOutput struct {
	Steps []struct {
		Name    string `json:"name"`
		Actions []struct {
			OutputURL string `json:"output_url"`
			Status    string `json:"status"`
		} `json:"actions"`
	} `json:"steps"`
}

type S3Output struct {
	Message string
}

type Result struct {
	Name    string `json:"name"`
	LogFile string `json:"logFile"`
	Status  string `json:"status"`
}

func checkURL() {
	var configFile = "./env.json"
	mu := sync.Mutex{}
	prevModTime := time.Time{}
	ticker := time.NewTicker(time.Second * 5)
	for ; true; <-ticker.C {
		stats, err := os.Stat(configFile)
		if err != nil {
			panic("wrong config in env.json provided")
		}
		currentModTime := stats.ModTime()
		if currentModTime == prevModTime {
			continue
		}
		prevModTime = currentModTime
		func() {
			mu.Lock()
			defer mu.Unlock()
			bytes, err := os.ReadFile(configFile)
			if err != nil {
				panic("wrong config in env.json provided")
			}
			err = json.Unmarshal(bytes, cfg)
			if err != nil || cfg.HasNullValues() {
				panic(fmt.Sprintf("wrong config in env.json provided: %s", err))
			}
		}()
	}
}

func deleteLogs() {
	ticker := time.NewTicker(time.Hour)
	for {
		select {
		case <-ticker.C:
			dirFiles, err := os.ReadDir("./logs")
			if err != nil {
				panic(err)
			}
			for _, file := range dirFiles {
				err := os.Remove("./logs/" + file.Name())
				if err != nil {
					panic(err)
				}
			}
		}
	}
}

func authMiddleware(hFn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()
		if !ok || user != cfg.BasicUser || pass != cfg.BasicPass {
			http.Error(w, "wrong username or password", http.StatusForbidden)
			return
		}
		hFn(w, r)
	}
}

func makeRunPipelineHandler(ghClient *github.Client) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		reqBody, err := io.ReadAll(r.Body)
		if err != nil {
			http.Error(w, "bad request", http.StatusBadRequest)
			return
		}
		r.Body.Close()
		m := make(map[string]interface{}, 0)
		err = json.Unmarshal(reqBody, &m)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		branch, ok := m["branch"].(string)
		if !ok {
			http.Error(w, "bad payload", http.StatusBadRequest)
			return
		}
		service := mux.Vars(r)["service"]

		ctx := context.Background()
		opts := &github.RepositoryContentFileOptions{
			Message:   github.String("fix: unblock the builds - step 1 of 2"),
			Content:   []byte(" "),
			Branch:    github.String(branch),
			Committer: &github.CommitAuthor{Name: github.String("Ivan Smaliakou"), Email: github.String(cfg.Email)},
		}
		content, _, err := ghClient.Repositories.CreateFile(ctx, cfg.RepoOwner, service, "unblock-build.txt", opts)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		opts.Message = github.String("fix: unblock the builds - step 2 of 2")
		opts.SHA = content.Content.SHA
		_, _, err = ghClient.Repositories.DeleteFile(ctx, cfg.RepoOwner, service, "unblock-build.txt", opts)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

func jobHandler(w http.ResponseWriter, r *http.Request) {
	varsMap := mux.Vars(r)
	service := varsMap["service"]
	jobID := varsMap["job_id"]
	request, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/v1.1/project/github/%s/%s/%s",
		cfg.CircleURL, cfg.RepoOwner, service, jobID), nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Circle-Token", cfg.CircleToken)
	request.Header.Add("Accept", "application/json")
	cl := &http.Client{}
	resp, err := cl.Do(request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	bytes, err := io.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	resp.Body.Close()
	var circleOutput CircleOutput
	err = json.Unmarshal(bytes, &circleOutput)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	result := make([]Result, 0, len(circleOutput.Steps))
	for _, step := range circleOutput.Steps {
		for _, action := range step.Actions {
			if len(action.OutputURL) == 0 {
				result = append(result, Result{
					Name:    step.Name,
					LogFile: "none",
					Status:  action.Status,
				})
				continue
			}
			resp, err := http.Get(action.OutputURL)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				log.Println(err.Error())
				return
			}
			filePath := "logs/" + gofakeit.UUID()
			file, err := os.Create(filePath)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				log.Println(err.Error())
				return
			}
			body, err := io.ReadAll(resp.Body)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				log.Println(err.Error())
				return
			}
			s3Out := make([]S3Output, 1)
			err = json.Unmarshal(body, &s3Out)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				log.Println(err.Error())
				return
			}
			_, err = file.WriteString(s3Out[0].Message)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				log.Println(err.Error())
				return
			}
			file.Close()

			fileUrl := cfg.ProtoAndHostname + `/` + filePath
			result = append(result, Result{
				Name:    step.Name,
				LogFile: fileUrl,
				Status:  action.Status,
			})
		}
	}
	b, err := json.MarshalIndent(&result, "", "    ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Println(err.Error())
		return
	}
	w.WriteHeader(200)
	_, err = w.Write(b)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Println(err.Error())
		return
	}
}

func main() {
	go checkURL()
	go deleteLogs()
	for cfg.HasNullValues() {
	}

	r := mux.NewRouter()
	fs := http.FileServer(http.Dir("./logs/"))
	ctx := context.Background()
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: cfg.GhToken},
	)
	tc := oauth2.NewClient(ctx, ts)

	ghClient := github.NewClient(tc)
	r.PathPrefix("/logs/").Handler(http.StripPrefix("/logs/", fs))
	r.HandleFunc("/{service}", authMiddleware(makeRunPipelineHandler(ghClient))).Methods(http.MethodPost)
	r.HandleFunc("/{service}/{job_id}", authMiddleware(jobHandler)).Methods(http.MethodGet)
	err := http.ListenAndServe("0.0.0.0:80", r)
	if err != nil {
		log.Fatalln(err)
	}
}
