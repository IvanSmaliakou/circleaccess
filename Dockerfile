FROM golang:1.18.1-alpine3.15
WORKDIR /app
COPY *.go go.* env.json ./
COPY logs/ ./logs

RUN go mod download
RUN go build -o bin/app .

EXPOSE 80/tcp

CMD ["bin/app"]
